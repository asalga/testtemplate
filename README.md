# TestTemplate - Andor Saga

Most agencies want some sort of coding test completed and I found myself re-writing the basic structure necessary to make builds. This is a solution to prevent having to rewrite the same code over and over again.

# Usage

Fork into a new repo and make any necessary changes.


# Building

## dev

```
$ grunt
```

## dist

```
$ grunt dist
```
