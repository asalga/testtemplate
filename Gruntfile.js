'use strict';


module.exports = function(grunt) {

  const Livereload = 35729;
  const ServeStatic = require('serve-static');

  const src = 'src';
  const dat = 'data';
  const tmp = '.tmp';
  const app = 'app';
  const dist = 'dist';
  const sassCache = '.sass-cache';
  const lang = grunt.option('lang') || 'en';

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    /**
     * https://www.npmjs.com/package/grunt-contrib-copy
     */
    copy: {
      dev: {
        files: [
          // HTML
          {
            expand: true,
            cwd: `${src}/`,
            src: `index.html`,
            dest: `${app}/`,
            filter: 'isFile'
          },
          // CSS
          {
            expand: true,
            cwd: `${src}/css`,
            src: `**`,
            dest: `${app}/css`,
            filter: 'isFile'
          },
          // FONTS
          {
            expand: true,
            cwd: `${src}/fonts`,
            src: `**/*.{woff,woff2,otf,ttf,eot}`,
            dest: `${app}/fonts`,
            filter: 'isFile'
          },
          // JS
          {
            expand: true,
            cwd: `${src}/js`,
            src: `*.js`,
            dest: `${app}/js`,
            filter: 'isFile'
          },
          // IMG
          {
            expand: true,
            cwd: `${src}/img`,
            src: `*.{png,jpg,jpeg,gif}`,
            dest: `${app}/img`,
            filter: 'isFile'
          }
        ]
      }
    },


    /**
     * https://github.com/jsoverson/grunt-open
     * Opens the web server in the browser
     */
    open: {
      server: {
        path: `http://localhost:<%= connect.options.port %>/`
      }
    },


    /** 
     */
    clean: {
      build: {
        src: [
          `${app}`,
          `${tmp}`,
          `${sassCache}`
        ]
      }
    },


    /**
     *
     */
    htmlhint: {
      dev: {
        options: {
          "attr-lowercase": true,
          "attr-value-double-quotes": true,
          "attr-value-not-empty": false,
          "attr-no-duplication": true,
          "alt-require": true,
          "attr-unsafe-chars": true,
          "doctype-html5": true,
          "id-unique": true,
          "style-disabled": false,
          "src-not-empty": true,
          "spec-char-escape": true,
          "space-tab-mixed-disabled": "space",
          "tagname-lowercase": true,
          "tag-pair": true,
          "title-require": false
        },
        src: [`${app}/index.html`]
      },
      dist: {
        options: {
          "attr-lowercase": true,
          "attr-value-double-quotes": true,
          "attr-value-not-empty": true,
          "attr-no-duplication": true,
          "alt-require": true,
          "attr-unsafe-chars": true,
          "doctype-html5": true,
          "id-unique": true,
          "style-disabled": false,
          "src-not-empty": true,
          "spec-char-escape": true,
          "space-tab-mixed-disabled": "space",
          "tagname-lowercase": true,
          "tag-pair": true,
          "title-require": false
        },
        src: [`${dist}/index.html`]
      }
    },

    /**
     * https://github.com/gruntjs/grunt-contrib-jshint
     * options inside .jshintrc file
     */
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },

      files: [
        `!${src}/js/lib/*.js`,
        `${src}/js/*.js`
      ]
    },


    /**
     * 
     */
    sass: {
      dev: {
        options: {
          style: 'nested',
          sourcemap: 'auto',
          update: true
        },
        files: [{
          dest: `${app}/css/style.css`,
          src: `${src}/scss/main-dev.scss`
        }]
      },
      dist: {
        options: {
          style: 'nested',
          sourcemap: 'auto',
          update: true
        },
        files: [{
          dest: `${app}/css/style.css`,
          src: `${src}/scss/main-dist.scss`
        }]
      }
    },

    /**
     * https://github.com/gruntjs/grunt-contrib-connect
     */
    connect: {
      options: {
        port: 9000,
        hostname: '*'
      },
      livereload: {
        options: {
          middleware: function(connect, options) {
            return [
              ServeStatic(`${app}`),
              connect().use(`${app}`, ServeStatic(`${app}`)),
              ServeStatic(`${app}`)
            ]
          }
        }
      }
    },

    /**
     * https://github.com/gruntjs/grunt-contrib-watch
     */
    watch: {
      options: {
        spawn: true,
        livereload: true
      },
      all: {
        files: [
          `${src}/**/*.*`
        ],
        tasks: [
          'jshint',
          'copy:dev',

          'loadCopyIntoObj',
          'replaceTask',

          'htmlhint',

          'sass:dev',
        ],
        options: {
          livereload: true
        }
      }
    }
  });

  /**
   * Load the copy into an object
   */
  grunt.registerTask('loadCopyIntoObj', function() {
    let copyMapping = grunt.file.readJSON(`${src}/data/copy.json`);

    let json = {
      'json': {}
    };

    for (let x in copyMapping) {
      json['json'][x] = copyMapping[x][lang];
    }
    grunt.file.write(`${tmp}/copy-${lang}.json`, JSON.stringify(json, null, 2));
  });

  /**
   *
   */
  grunt.registerTask('replaceTask', function() {
    grunt.config('replace', {
      default: {
        options: {
          patterns: [
            grunt.file.readJSON(`${tmp}/copy-${lang}.json`)
          ]
        },
        files: [{
          expand: true,
          flatten: true,
          src: `${app}/index.html`,
          dest: `${app}`
        }]
      },
    });
    grunt.task.run('replace');
  });

  /*
   *
   */
  grunt.registerTask('default', [
    'jshint',
    'copy:dev',

    'loadCopyIntoObj',
    'replaceTask',

    'htmlhint',

    'sass:dev',

    // LIVE UPDATES / PREVIEW
    'connect:livereload',
    'open',
    'watch'
  ]);

  grunt.registerTask('dist', [
    // VALIDATION
    'jshint',
    'htmlhint',

    'copy',

    // SCSS
    'sass:dist'
  ]);
};